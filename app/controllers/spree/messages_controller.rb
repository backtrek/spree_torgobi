class Spree::MessagesController < ApplicationController
    include Spree::Core::ControllerHelpers::Store
    include Spree::Core::ControllerHelpers::Common
    include Spree::Core::ControllerHelpers::Currency
    helper Spree::BaseHelper
    invisible_captcha only: [:create], honeypot: :subtitle, on_spam: :spam_callback_method

    def index

    end

    def new
      @message = Message.new
    end

    def create
        @message = Message.new(message_params)
    
        if @message.save
          flash[:success] = Spree.t('message.message_sent')
          redirect_back fallback_location: root_path
        else 
          flash[:error] = Spree.t('message.message_not_sent')
          render :new
        end
    end

    private
    def message_params
      params.require(:message).permit(:name, :notification_email, :subject, :body, :newsletter)
    end

    def spam_callback_method
      redirect_to root_path
    end
end
