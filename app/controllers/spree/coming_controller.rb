class Spree::ComingController < ApplicationController
  include Spree::Core::ControllerHelpers::Store
  include Spree::Core::ControllerHelpers::Common
  include Spree::Core::ControllerHelpers::Currency
  helper Spree::BaseHelper
  
  def index
    @subscription = Spree::Subscription.new
  end
end
