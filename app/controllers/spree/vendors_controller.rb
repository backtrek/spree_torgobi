class Spree::VendorsController < ApplicationController
  include Spree::Core::ControllerHelpers::Auth
  include Spree::Core::ControllerHelpers::Common
  include Spree::Core::ControllerHelpers::Store
  include Spree::Admin::VendorHelper
  include Spree::Core::ControllerHelpers::Currency
  helper Spree::BaseHelper
  helper Spree::ProductsHelper

  rescue_from ActiveRecord::RecordNotFound, with: :resource_not_found
  rescue_from ActiveRecord::RecordNotUnique, with: :record_not_unique
  
  def index
    #the shops page will only list active sellers
    @vendors = Spree::Vendor.active.all
  end

  def show
    #@vendor = Spree::Vendor.find(params[:id])
    @vendor = Spree::Vendor.friendly.find(params[:id])
    @products = @vendor.products

    #set a taxon in order to avoid the display of products.jpg in spree/products/index
    @taxon = Spree::Taxon.find_by_name('Buy')
  end

  def new
    if try_spree_current_user
      if current_spree_vendor
        case current_spree_vendor.state
          when 'active' then redirect_to admin_path #redirects to the admin page
          when 'pending' then flash[:warning] = Spree.t('seller.seller_request_pending')
            redirect_to vendor_path(current_spree_vendor) #redirects to the seller details page
          when 'blocked' then flash[:warning] = Spree.t('seller.seller_request_blocked')
            redirect_to vendor_path(current_spree_vendor) #redirects to the seller details page
        end
      else
        @vendor = Spree::Vendor.new
        #email text field will be pre-filled with current user's email address
        @vendor.notification_email = current_spree_user.email
      end
    else
      store_location #sets session return url
      flash[:warning] = Spree.t('seller.user_account')
      redirect_to create_new_session_path #redirects to login page
    end
  end

  def create
    #all attributes except image is attached to the vendor model
    @vendor = Spree::Vendor.new(vendor_params.except(:image))

    #all attributes (input text from the form) except the image are saved in the database
    #@vendor.attributes = vendor_params.except(:image)

    #image is attached to the vendor model
    if vendor_params[:image] && Spree.version.to_f >= 3.6
      @vendor.build_image(attachment: vendor_params[:image])
    end

    respond_to do |format|
      if @vendor.save
        map_vendor
        flash[:success] = Spree.t('seller.seller_request_submitted')
        format.html { redirect_to vendor_path(current_spree_vendor) }
        format.js { render layout: false }
      else
        format.html { render :new }
        format.js { render layout: false }
      end
    end
  end

  private

  def vendor_params
    #params.require(:vendor).permit(:name, :about_us, :contact_us, :image)
    params.require(:vendor).permit(Spree::PermittedAttributes.vendor_attributes)
  end

  def map_vendor
    #mapping the spree user with the seller
    current_spree_user.vendors << @vendor
  end

  def resource_not_found
    flash[:error] = Spree.t('seller.resource_not_found')
    redirect_to root_path
  end

  #This method catches the ActiveRecord::RecordNotUnique exception. The exception is thrown when a record already exists in the database but marked as deleted.
  def record_not_unique
    #An additional error message is added to the vendor model if the seller name already exists as part of a record marked as deleted. 
    @vendor.errors.add(:name, :not_unique, message: Spree.t('seller.name_taken'))
    render :new
  end
end