class Spree::SubscriptionsController < ApplicationController
  include Spree::Core::ControllerHelpers::Store
  include Spree::Core::ControllerHelpers::Common
  include Spree::Core::ControllerHelpers::Currency
  helper Spree::BaseHelper

  def index
  end
  
  def new
    @subscription = Spree::Subscription.new
  end

  def create
      @subscription = Spree::Subscription.new(subscription_params)
  
      if @subscription.save
        flash[:success] = Spree.t('message.message_sent')
        redirect_back fallback_location: root_path
      else 
        flash[:error] = Spree.t('message.message_not_sent')
        render :new
      end
  end

  private
  def subscription_params
    params.require(:subscription).permit(:name, :notification_email)
  end
end
