class Message < ApplicationRecord
    validates :name, presence: true
    validates :notification_email, presence: true#, uniqueness: true, format: { with: URI::MailTo::EMAIL_REGEXP }
    validates :subject, presence: true
    validates :body, presence: true
end
