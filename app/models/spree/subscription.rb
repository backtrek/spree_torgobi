class Spree::Subscription < ApplicationRecord
    validates :name, presence: true
    validates :notification_email, presence: true, uniqueness: true
end
