Spree::Core::Engine.add_routes do
  # Add your extension routes here
  resources :vendors, controller: 'vendors'
  get "/sell" => "vendors#new"
  get "/shops" => "vendors#index"
  get "/about" => "about#index"
  resources :messages, controller: 'messages'
  get "/contact" => "messages#new"
  get "/coming" => "coming#index"
  resources :subscriptions
end
