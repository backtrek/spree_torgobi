class CreateSpreeSubscriptions < ActiveRecord::Migration[6.1]
  def change
    create_table :spree_subscriptions do |t|
      t.string :name
      t.string :notification_email

      t.timestamps
    end
  end
end
